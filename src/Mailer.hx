using mw.Assertions;

class Mailer {

#if MAILER_PHP_MAIL_FUNCTION
  public static function send(email:Email) {
	email.toList.length.assert_eq(1);
	var to:String = email.toList.first();
	switch (email.content) {
	  case text(text):
		php.Lib.mail(to, email.subject, text);
	}
  }
#end

}
